## Preparation
Project is dependent on _npm_ and _bower_.  
On the development machine, install [Node.js with npm](https://nodejs.org/en/) (suggested LTS-version at the time of writing is 6.11).

# How to develop locally

Start by installing dependencies with npm   
`$ npm install`

## Must be runned through webserver
For ajax-calls to work properly the project must be runned through a server. When published to eg Bluemix this will not be a problem but developing locally the built-in express-server must be used.  
Starting the app-server is done by triggering the _start_ script from _package.json_ by in the project folder type

`$ npm start`    

Then project is visible on `localhost:[defined-port-in-settings]`

## Settings
Settings is defined in _settings.js_ in the root of the project.

## Add new npm dependencies (when needed)
`$ npm install [PACKAGE] --save`

## Add new bower dependencies (when needed)
`$ bower install [PACKAGE] --save`

Reference in HTML    
`<script src="bower_components/[PACKAGE-PATH]"></script>`

# How to deploy in Bluemix  
 - Download, install and setup (login and targeting correct org etc) [Bluemix CLI](https://clis.ng.bluemix.net/)

 - Edit the _manifest.yaml_ with relevant data (especially the _name_ and a unique _host_/_route_).

 - In the root of the folder, open a commandprompt and type `bx app push `.

  - In web browser go to your app: `[choosed-route]` 

 [CLI documentation](https://console.bluemix.net/docs/cli/index.html)


## Optionally
Create a toolchain to integrate Bluemix and git so that for example a new _push_ to the repo (on Github or Bitbucket) would trigger a build on Bluemix (and thus the _bx app push_ is superfluous).

# Folderstructure
- **/** - (the project root)  
Contains settings for bluemix, npm, Bower and the application itself. And also _server.js_, the serverside code Node/Express-server.

- **/public**  
This folder is visible as the GUI (i.e. when entering the webaddress). This is the place for all the clientside code like html, css and javascript. _No hardcoded passwords in this folder!_

- **/api**  
Serverside code for the api which is mouned by _server.js_ under the _[webaddress]/api_ endpoint.

- **/shared**  
Common shared files for the serverside code.

# API

Base url: registerbrevladan.lm-watson.se

Documented through Swagger (http://localhost:8081/swagger). Using application key in header to avoid misuse.  
[settings.appIdHeaderKey]: [settings.appId]

# Loggar
## Backend
Loggar generaras genom biblioteket [`winston`](https://github.com/winstonjs/winston) och med rätt [_Transport_](https://github.com/winstonjs/winston/blob/master/docs/transports.md#winston-more) kan de skickas till loggfil, konsol, Kibana eller annan logtjänst (molnbaserad, tex logz.io).  
Den inbyggda log-motorn generarar (så länge den används) automagiskt loggar innehållandes

- **origin**: ip-adress för inkommande anrop

- **environment**: 'development' om applicationen körs lokalt, 'production' om den körs i IBM Cloud samt 'production_beta' om den körs i IBM Cloud ach appnamnet har ordet 'beta' i sig (tex betabrevlådan).

- **label**: Namnet på applicationen (specificerat i package.json), alternativt 'local_dev' om namnet ej kan läsas.

I koden är loggarn attachad till _req_-objektet och anropas genom

```req.logger('This is the message')```

eller

```req.logger('This is the message', {param1:'extra parameter 1, param2:'extra parameter 2})```

Loggningen sker till konsol, fil samt logz.io. Filens namn är `logstash.log` och är åtkomlig genom _/api/log_

## Frontend
Är det nödvändigt? Blir bara bökigt och onödigt extrajobb?