function dropHandler(event){
    console.log("Files dropped");
    event.preventDefault();
    
   
    
    if(event.dataTransfer.files){
        console.log(event.dataTransfer.files);
        
        for(var i = 0; i < event.dataTransfer.files.length; i++){
            console.log(event.dataTransfer.files[i].name);
        }
    }
    
    removeDragData(event);
    
}

function dragOverHandler(event){
    event.preventDefault();
}

function removeDragData(ev) {
  console.log('Removing drag data')

  if (ev.dataTransfer.items) {
    // Use DataTransferItemList interface to remove the drag data
    ev.dataTransfer.items.clear();
  } else {
    // Use DataTransfer interface to remove the drag data
    ev.dataTransfer.clearData();
  }
}