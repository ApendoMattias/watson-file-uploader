var express = require('express'),
    settings = require('./settings'),
    bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json({limit: '5mb'}));       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true, limit: '5mb'
}));

// require appID for all routes
app.use('/api', function (req, res, next) {

    if (req.get(settings.appIdHeaderKey) == settings.appId) {
        next();
    }
    else {
        res.status(401).send('Please provide valid application key')
    }

});

// Prepare for SPA in 'public' folder
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use('/', express.static(__dirname + '/public'));

// Start the server
var port = process.env.PORT || settings.port;
app.listen(port, function () {
    console.log('Server listening on localhost:' + port);
});