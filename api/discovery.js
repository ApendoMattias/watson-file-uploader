var express = require('express'),
    settings = require('../settings'),
    watson = require('watson-developer-cloud'),
    winston = require('winston'),
	request = require('request');

var router = express.Router();

var discovery = watson.discovery({
    username: settings.watson_discovery.username,
    password: settings.watson_discovery.password,
    version: settings.watson_discovery.version,
    version_date: settings.watson_discovery.version_date
});

router.post('/', discover);
router.post('/train', discoverTraining);
router.get('/trainingQuestions', trainingQuestions);
router.post('/relevance',logRelevance);

module.exports = router;

function logRelevance(req){
    // Log relevance and user id
    req.logger("Customer response", {UserID : req.headers.userid, Question : req.body.userInput.question, RelevantAnswer : req.body.userInput.isRelevant, NotRelevantAnswer : req.body.userInput.isNotRelevant});
}

function discoverTraining(req,res){
    // Använder Winston för att logga träningsdata som skickas in från användarna
    req.logger("Training logging: ", {UserID : req.headers.userId, Question : req.body.training_data.natural_language_query, Svar : req.body.training_data.answers, AntalSvar : req.body.training_data.answersLength, relevantaSvar : req.body.training_data.thumbsUpCount, ickeRelevantaSvar : req.body.training_data.thumbsDownCount});
        
    request({
            method: 'post',
            json: true,
            uri: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/' + settings.watson_discovery.environment + '/collections/' + settings.watson_discovery.collection + '/training_data?version=2017-11-07',
            body: req.body.training_data
        },
        function (error, response, body) {
            if (error) {
                res.send(error);
            }
            res.send(body);
        }
    ).auth(settings.watson_discovery.username, settings.watson_discovery.password);
}

function trainingQuestions(req,res) {
	//console.log(req.body);
    request({
            method: 'get',
            json: true,
            uri: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/' + settings.watson_discovery.environment + '/collections/' + settings.watson_discovery.collection + '/training_data?version=2017-11-07'
        },
        function (error, response, body) {
            if (error) {
                res.send(error);
            }
            res.send(body);
			//console.log('Bodyn: ',body.queries.length);
        }
    ).auth(settings.watson_discovery.username, settings.watson_discovery.password);
}

// Call our collection with question from the user
function discover(req, res) {
    
    // Log the question
    req.logger("Start querying Discovery service ",{UserID : req.headers.userid, Question : req.body.natural_language_query});

    if (req.body.natural_language_query == null)
        return res.status(400).send('Please set correct parameters');

    // Make settings for the call here
    var query_opts = {
        environment_id: settings.watson_discovery.environment,
        collection_id: settings.watson_discovery.collection,
        natural_language_query: req.body.natural_language_query || '',
        passages: true,
        highlight: true
    };

    // Send the question and return the answers to the user
    discovery.query(query_opts, function (error, data) {
        if(error){
            req.logger("Error in query request",{Error : error});
            res.send(error)
        }
        res.send(data);
    });
}
